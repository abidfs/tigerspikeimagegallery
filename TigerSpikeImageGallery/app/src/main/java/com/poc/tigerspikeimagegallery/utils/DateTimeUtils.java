package com.poc.tigerspikeimagegallery.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility class for date manipulation.
 * <p>
 * Created by SAYYAD on 18-12-2016.
 */
public class DateTimeUtils {

    /**
     * Method to compare dates
     *
     * @param strDateLeft  the string date
     * @param strDateRight the string date
     * @param dateFormat   the {@link SimpleDateFormat} object
     * @return an int < 0 if this strDateLeft is less than strDateRight, 0 if they are equal, and an int > 0 if this strDateLeft is greater.
     */
    public static int compareDates(String strDateLeft, String strDateRight, SimpleDateFormat dateFormat) {
        try {
            Date dateLeft = dateFormat.parse(strDateLeft);
            Date dateRight = dateFormat.parse(strDateRight);
            return dateLeft.compareTo(dateRight);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
