package com.poc.tigerspikeimagegallery.communication.webservices;

import com.poc.tigerspikeimagegallery.MainApplication;
import com.poc.tigerspikeimagegallery.R;
import com.poc.tigerspikeimagegallery.communication.CommunicationConstants;
import com.poc.tigerspikeimagegallery.communication.CommunicationResponse;
import com.poc.tigerspikeimagegallery.communication.HttpCommunication;
import com.poc.tigerspikeimagegallery.communication.RequestParam;
import com.poc.tigerspikeimagegallery.utils.NetworkUtils;

import java.util.ArrayList;

/**
 * Abstract base class for all the webservices.
 * All webservice class must extend this class and provide the definitions for all abstract methods.
 * This class used {@link HttpCommunication} object to send the request and get the response from server.
 * The http request is sent from background thread.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public abstract class WebServiceBase {
    /**
     * Method to get the communication url/endpoint
     *
     * @return the communication url/endpoint
     */
    protected abstract String getUrl();

    /**
     * Method to get the list of communication params.
     *
     * @return the {@link ArrayList} of {@link RequestParam} objects
     */
    protected abstract ArrayList<RequestParam> getRequestParams();

    /**
     * Method to get the unique webservice code for this webservice.
     *
     * @return the unique webservice code for this webservice.
     */
    protected abstract int getRequestCode();

    /**
     * Method to get the {@link com.poc.tigerspikeimagegallery.communication.HttpCommunication.CommunicationMethod}
     *
     * @return the {@link com.poc.tigerspikeimagegallery.communication.HttpCommunication.CommunicationMethod}
     */
    protected abstract HttpCommunication.CommunicationMethod getCommunicationMethod();

    /**
     * Method to parse the communication response received from server and notify event to application component calling this webservice.
     *
     * @param response the {@link CommunicationResponse} object
     */
    protected abstract void parseResponse(CommunicationResponse response);

    /**
     * Method to notify error to application component calling this webservice.
     *
     * @param error the {@link CommunicationResponse} object
     */
    protected abstract void notifyError(CommunicationResponse error);

    /**
     * Method to send the HTTP request from background thread.
     * First checks if network is available. If not, notifies error network unavailable event and returns.
     * Request is only sent if network is available.
     */
    public void send() {
        if (!NetworkUtils.isNetworkAvailable()) {
            CommunicationResponse error = new CommunicationResponse(getRequestCode(), CommunicationConstants.ERROR_NO_NETWORK, MainApplication.appContext.getString(R.string.errNetworkUnavailable));
            notifyError(error);
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpCommunication httpCommunication = new HttpCommunication();
                    String url = getUrl();
                    ArrayList<RequestParam> requestParams = getRequestParams();
                    int requestCode = getRequestCode();
                    HttpCommunication.CommunicationMethod method = getCommunicationMethod();
                    String strResponse = httpCommunication.makeHttpRequest(url, method, requestParams);

                    CommunicationResponse response = new CommunicationResponse(requestCode, strResponse);
                    parseResponse(response);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    CommunicationResponse error = new CommunicationResponse(getRequestCode(), CommunicationConstants.ERROR_COMMUNICATION, MainApplication.appContext.getString(R.string.errInCommunication, ex.getMessage()));
                    notifyError(error);
                }
            }
        }).start();
    }
}