package com.poc.tigerspikeimagegallery.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.poc.tigerspikeimagegallery.datamodels.ImageFeedItem;
import com.poc.tigerspikeimagegallery.ui.fragments.FullScreenImageFragment;

import java.util.ArrayList;

/**
 * Adapter class for full screen image {@link android.support.v4.view.ViewPager}
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class FullScreenImagePagerAdapter extends FragmentPagerAdapter {
    private ArrayList<ImageFeedItem> imageFeedItems;

    public FullScreenImagePagerAdapter(FragmentManager fm, ArrayList<ImageFeedItem> imageFeedItems) {
        super(fm);
        this.imageFeedItems = imageFeedItems;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return FullScreenImageFragment.newInstance(imageFeedItems.get(position), position);
    }

    @Override
    public int getCount() {
        return imageFeedItems.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return imageFeedItems.get(position).getTitle();
    }
}
