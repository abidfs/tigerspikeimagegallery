package com.poc.tigerspikeimagegallery.communication;

/**
 * Datamodel class used to hold the communication response related information.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class CommunicationResponse {
    private Object responseObject;
    private int requestCode;
    private int errorCode;
    private String errorDescription;
    private Object extra;

    /**
     * Parameterized Constructor
     *
     * @param requestCode
     * @param responseObject
     */
    public CommunicationResponse(int requestCode, Object responseObject) {
        this.requestCode = requestCode;
        this.responseObject = responseObject;
    }

    /**
     * Parameterized Constructor
     *
     * @param requestCode
     * @param errorCode   the error code
     * @param errorDescription the error errorDescription
     */
    public CommunicationResponse(int requestCode, int errorCode, String errorDescription) {
        this.requestCode = requestCode;
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    /**
     * Parameterized Constructor
     *
     * @param requestCode
     * @param errorCode   the error code
     * @param errorDescription the error errorDescription
     * @param extra       the optional error extra
     */
    public CommunicationResponse(int requestCode, int errorCode, String errorDescription, Object extra) {
        this.requestCode = requestCode;
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
        this.extra = extra;
    }

    public int getRequestCode() {
        return requestCode;
    }

    /**
     * @return the errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the errorDescription
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * @param errorDescription the errorDescription to set
     */
    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    /**
     * @return the extra
     */
    public Object getExtra() {
        return extra;
    }

    /**
     * @param extra the extra to set
     */
    public void setExtra(String extra) {
        this.extra = extra;
    }

    public Object getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(Object responseObject) {
        this.responseObject = responseObject;
    }
}
