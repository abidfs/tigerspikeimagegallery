package com.poc.tigerspikeimagegallery.ui.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.poc.tigerspikeimagegallery.R;
import com.poc.tigerspikeimagegallery.datamodels.ImageFeedItem;

import java.util.ArrayList;

/**
 * This class is used as adapter for displaying the Image Gallery from feeds.
 * For each item it displays title and a thumbnail.
 * It loads the thumbnail using Glide library for smooth scrolling of list and rich user experience.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class ImageGalleryAdapter extends RecyclerView.Adapter<ImageGalleryAdapter.ImageGalleryViewHolder> {

    private ArrayList<ImageFeedItem> feedItems;
    private final LayoutInflater inflater;
    private final View.OnClickListener onClickListener;

    /**
     * Parameterized Constructor.
     *
     * @param activity        the {@link Activity} context
     * @param feedItems       the {@link ArrayList} of {@link ImageFeedItem} objects
     * @param onClickListener the {@link android.view.View.OnClickListener} to handle thumbnail and download image clicks
     */
    public ImageGalleryAdapter(Activity activity, ArrayList<ImageFeedItem> feedItems, View.OnClickListener onClickListener) {
        this.feedItems = feedItems;
        inflater = LayoutInflater.from(activity);
        this.onClickListener = onClickListener;
    }

    public ArrayList<ImageFeedItem> getFeedItems() {
        return feedItems;
    }

    /**
     * Method to update the feed items and notify data set changed events so that list will be refreshed
     *
     * @param feedItems the {@link ArrayList} of {@link ImageFeedItem} objects
     */
    public void updateDataset(ArrayList<ImageFeedItem> feedItems) {
        this.feedItems = feedItems;
        notifyDataSetChanged();
    }

    @Override
    public ImageGalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.image_gallery_list_item, parent, false);
        ImageGalleryViewHolder viewHolder = new ImageGalleryViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ImageGalleryViewHolder holder, int position) {
        ImageFeedItem feedItem = feedItems.get(position);
        holder.tvTitle.setText(feedItem.getTitle());
        holder.tvAuthor.setText(feedItem.getAuthor());

        Glide.with(holder.ivThumbnail.getContext()).load(feedItem.getThumbnailUrl())
                /*.thumbnail(0.25f)*/
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivThumbnail);

        //Attach onClickListener to thumbnail image.
        //Click will be handled by activity to start image in full screen.
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(onClickListener);

        //Attach onclickListener to download icon.
        //Click will be handled by activity to take the necessary action.
        holder.ivDownload.setTag(feedItem);
        holder.ivDownload.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        if (feedItems != null)
            return feedItems.size();
        else
            return 0;
    }

    /**
     * {@link android.support.v7.widget.RecyclerView.ViewHolder} class for ImageGallery
     */
    class ImageGalleryViewHolder extends RecyclerView.ViewHolder {
        ImageView ivThumbnail, ivDownload;
        TextView tvTitle, tvAuthor;

        public ImageGalleryViewHolder(View itemView) {
            super(itemView);
            ivThumbnail = (ImageView) itemView.findViewById(R.id.ivThumbnail);
            ivDownload = (ImageView) itemView.findViewById(R.id.ivDownload);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvAuthor = (TextView) itemView.findViewById(R.id.tvAuthor);
        }
    }
}