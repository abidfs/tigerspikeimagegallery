package com.poc.tigerspikeimagegallery.ui.utils;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * Utility class to display and hide progress dialog.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class CustomProgressDialog {
    private static ProgressDialog progressDialog;

    /**
     * Method to show progress dialog.
     *
     * @param activity    the activity context.
     * @param message     the message
     * @param cancellable boolean indicating whether the progress dialog is cancelable
     */
    public static void show(final Activity activity, final String message, boolean cancellable) {
        try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        progressDialog = new ProgressDialog(activity);
                        progressDialog.setMessage(message);
                        progressDialog.setCancelable(false);
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to dismiss the progress dialog.
     *
     * @param activity the activity context
     */
    public static void dismiss(final Activity activity) {
        try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}