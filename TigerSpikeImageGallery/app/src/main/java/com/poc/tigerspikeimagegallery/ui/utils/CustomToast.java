package com.poc.tigerspikeimagegallery.ui.utils;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;


/**
 * Custom toast class. Can be used if we want to have a customized toast throughout the application.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class CustomToast {
    private static final String TAG = CustomToast.class.getSimpleName();
    public static final int LENGTH_LONG = Toast.LENGTH_LONG;
    public static final int LENGTH_SHORT = Toast.LENGTH_SHORT;

    /**
     * Method to show the toast message
     *
     * @param activity the activity context
     * @param message  the string
     * @param length   the toast length
     */
    public static void show(final Activity activity, final String message, final int length) {
        try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Toast.makeText(activity, message, length).show();
                    } catch (Exception e) {
                        Log.e(TAG, "show: run: Exception->" + e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "show: Exception->" + e.getMessage());
            e.printStackTrace();
        }
    }
}
