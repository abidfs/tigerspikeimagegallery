package com.poc.tigerspikeimagegallery;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * The application class.
 * Used to initialize crashlytics and holding the application context.
 * Created by SAYYAD on 16-12-2016.
 */
public class MainApplication extends Application {
    public static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        Fabric.with(this, new Crashlytics());
    }
}
