package com.poc.tigerspikeimagegallery.communication;

/**
 * This class holds different constants required for communication.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class CommunicationConstants {
    public static final int ERROR_COMMUNICATION = -1001;
    public static final int ERROR_NO_NETWORK = -1002;
    public static final int ERROR_INVALID_RESPONSE = -1003;

    public static final int GET_FLICKR_FEEDS = 101;
    public static final int GET_FEED_MAIN_IMAGE_URL = 102;

}
