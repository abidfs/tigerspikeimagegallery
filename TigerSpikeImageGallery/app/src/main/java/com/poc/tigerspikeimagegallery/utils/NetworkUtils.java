package com.poc.tigerspikeimagegallery.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.poc.tigerspikeimagegallery.MainApplication;

/**
 * Utility class to check if network is available
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class NetworkUtils {
    /**
     * Method to check if Wifi/Mobile network is available
     *
     * @return true if available, false otherwise
     */
    public static boolean isNetworkAvailable() {
        return isMobileNetworkAvailable() || isWifiNetworkAvailable();
    }

    /**
     * Method to check if Mobile network is available
     *
     * @return true if available, false otherwise
     */
    private static boolean isMobileNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) MainApplication.appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null)
            return networkInfo.isConnectedOrConnecting();

        return false;
    }

    /**
     * Method to check if Wifi network is available
     *
     * @return true if available, false otherwise
     */
    private static boolean isWifiNetworkAvailable() {
        WifiManager wifiManager = (WifiManager) MainApplication.appContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo != null)
            return wifiInfo.getSupplicantState() == SupplicantState.COMPLETED;

        return false;
    }
}
