package com.poc.tigerspikeimagegallery.utils;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;

import com.poc.tigerspikeimagegallery.MainApplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Utility class for caching the images to gallery.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class ImageCacheUtils {
    /**
     * Method to cache the input bitmap to gallery.
     *
     * @param bitmap   the image {@link Bitmap} to cache
     * @param fileName the file name to cache with
     */
    public static void saveImageToGallery(Bitmap bitmap, String fileName) {
        File picturesDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File cachedImageFile = new File(picturesDirectory, fileName);
        try {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(cachedImageFile));

            //Notify media scan broadcast, so that image will be available to gallery
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(cachedImageFile);
            mediaScanIntent.setData(contentUri);
            MainApplication.appContext.sendBroadcast(mediaScanIntent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
