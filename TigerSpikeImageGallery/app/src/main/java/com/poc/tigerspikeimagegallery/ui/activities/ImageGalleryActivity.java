package com.poc.tigerspikeimagegallery.ui.activities;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.poc.tigerspikeimagegallery.R;
import com.poc.tigerspikeimagegallery.communication.CommunicationConstants;
import com.poc.tigerspikeimagegallery.communication.CommunicationResponse;
import com.poc.tigerspikeimagegallery.communication.ICommunicationCallback;
import com.poc.tigerspikeimagegallery.communication.webservices.GetFeedMainImageUrl;
import com.poc.tigerspikeimagegallery.communication.webservices.GetFlickrFeeds;
import com.poc.tigerspikeimagegallery.datamodels.ImageFeedItem;
import com.poc.tigerspikeimagegallery.ui.adapters.ImageGalleryAdapter;
import com.poc.tigerspikeimagegallery.ui.fragments.FilterDialogFragment;
import com.poc.tigerspikeimagegallery.ui.utils.CustomAlertDialog;
import com.poc.tigerspikeimagegallery.ui.utils.CustomProgressDialog;
import com.poc.tigerspikeimagegallery.ui.utils.CustomToast;
import com.poc.tigerspikeimagegallery.ui.utils.UiUtilities;
import com.poc.tigerspikeimagegallery.utils.DateTimeUtils;
import com.poc.tigerspikeimagegallery.utils.ImageCacheUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Activity class to show the Gallery view.
 * Loads feeds using flickr public APIs and shows them in grid format.
 * Allows the user to search feeds by tags.
 * Allows the user to download any feed image.
 * Allows the user to sort feeds by dateTaken/datePublished in ascending/descending order.
 */
public class ImageGalleryActivity extends AppCompatActivity implements View.OnClickListener, ICommunicationCallback {
    private static final String TAG = ImageGalleryActivity.class.getSimpleName();
    private static final int RC_SD_CARD_PERMISSION = 1001;
    private LinearLayout llNoImages;
    private RecyclerView rvImageGallery;
    private ImageGalleryAdapter adapter;

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    private Handler uiUpdateHandler;
    private final int MSG_UPDATE_LIST = 101;
    private final int MSG_DOWNLOAD_FEED_MAIN_IMAGE = 102;

    private FilterDialogFragment dialogFilter;

    public enum SortBy {
        DATE_TAKEN, DATE_PUBLISHED
    }

    public enum SortOrder {
        ASCENDING, DESCENDING
    }

    private SortBy sortBy = null;
    private SortOrder sortOrder = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gallery);

        initializeViews();
        initializeUiUpdateHandler();

        getFeedsFromServer(null);
    }

    /**
     * Method to initialize views.
     * Called once, when the activity gets created.
     */
    private void initializeViews() {
        llNoImages = (LinearLayout) findViewById(R.id.llNoImages);
        llNoImages.setVisibility(View.VISIBLE);

        RecyclerView.LayoutManager gridLayoutManager = new GridLayoutManager(ImageGalleryActivity.this, 2);
        rvImageGallery = (RecyclerView) findViewById(R.id.rvImageGallery);
        rvImageGallery.setVisibility(View.GONE);
        rvImageGallery.setLayoutManager(gridLayoutManager);
        rvImageGallery.setItemAnimator(new DefaultItemAnimator());
    }

    /**
     * Method to update the feed list.
     *
     * @param feedItems the {@link ArrayList} of {@link ImageFeedItem} objects
     */
    private void updateList(ArrayList<ImageFeedItem> feedItems) {
        if (feedItems.size() > 0) {
            llNoImages.setVisibility(View.GONE);
            rvImageGallery.setVisibility(View.VISIBLE);
            if (adapter == null) {
                //Initialize the adapter if feeds are received for the first time
                adapter = new ImageGalleryAdapter(ImageGalleryActivity.this, feedItems, ImageGalleryActivity.this);
                rvImageGallery.setAdapter(adapter);
            } else {
                //Update the adapter if feeds are received after adding/removing tags
                adapter.updateDataset(feedItems);
            }
        } else {
            llNoImages.setVisibility(View.VISIBLE);
            rvImageGallery.setVisibility(View.GONE);
        }
    }

    /**
     * Method to initialize the UI update handler.
     * Called when the webservice response is received in case of;
     * 1. Feed list
     * 2. Feed details
     */
    private void initializeUiUpdateHandler() {
        uiUpdateHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                switch (message.what) {
                    case MSG_UPDATE_LIST:
                        try {
                            CustomProgressDialog.dismiss(ImageGalleryActivity.this);
                            updateList((ArrayList<ImageFeedItem>) message.obj);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;

                    case MSG_DOWNLOAD_FEED_MAIN_IMAGE:
                        try {
                            CustomProgressDialog.dismiss(ImageGalleryActivity.this);
                            downloadFeedImage((ImageFeedItem) message.obj);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_image_gallery, menu);

        MenuItem searchItem = menu.findItem(R.id.menuSearch);
        searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "onQueryTextSubmit: tags->" + query);
                if (!TextUtils.isEmpty(query)) {
                    searchView.clearFocus();
                    UiUtilities.hideSoftKeyboard(searchView);
                    CustomProgressDialog.show(ImageGalleryActivity.this, getString(R.string.msgSearching), false);
                    getFeedsFromServer(query);
                }
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);

        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuRefresh:
                getFeedsFromServer(null);
                break;

            case R.id.menuFilter:
                showFilterDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method to get the list for feeds from server.
     *
     * @param optionalTags the space separated list of tags
     */
    private void getFeedsFromServer(String optionalTags) {
        CustomProgressDialog.show(ImageGalleryActivity.this, getString(R.string.msgLoadingFeeds), false);

        GetFlickrFeeds getFlickrFeeds;
        if (TextUtils.isEmpty(optionalTags)) {
            getFlickrFeeds = new GetFlickrFeeds(ImageGalleryActivity.this);
            getFlickrFeeds.send();
        } else {
            getFlickrFeeds = new GetFlickrFeeds(optionalTags, ImageGalleryActivity.this);
        }
        getFlickrFeeds.send();
    }

    /**
     * Method to get feed main image url and other details from server.
     *
     * @param imageFeedItem the {@link ImageFeedItem} object
     */
    private void getFeedMainImageUrl(ImageFeedItem imageFeedItem) {
        CustomProgressDialog.show(ImageGalleryActivity.this, getString(R.string.msgGettingFeedImageUrl), false);
        GetFeedMainImageUrl getFeedMainImageUrl = new GetFeedMainImageUrl(imageFeedItem, ImageGalleryActivity.this);
        getFeedMainImageUrl.send();
    }

    /**
     * Method to show filter dialog.
     */
    private void showFilterDialog() {
        dialogFilter = new FilterDialogFragment();
        dialogFilter.show(getSupportFragmentManager(), "FilterDialogFragment");
        dialogFilter.setOnClickListener(ImageGalleryActivity.this);
    }

    /**
     * Method to hide filter dialog
     */
    public void dismissFilterDialog() {
        if (dialogFilter != null && dialogFilter.isAdded()) {
            dialogFilter.dismiss();
            dialogFilter = null;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivDownload:
                //Check if WRITE_EXTERNAL_STORAGE permission is granted
                if (canWriteToSdCard()) {
                    ImageFeedItem item = (ImageFeedItem) view.getTag();
                    String imageUrl = item.getImageUrl();
                    if (TextUtils.isEmpty(imageUrl)) {
                        getFeedMainImageUrl(item);
                    } else {
                        downloadFeedImage(item);
                    }
                } else {
                    //Request runtime permission WRITE_EXTERNAL_STORAGE
                    requestWriteSDCardPermission();
                }
                break;

            case R.id.flImageGalleryListItem:
                Intent fullScreenImageIntent = new Intent(ImageGalleryActivity.this, FullScreenImageActivity.class);
                fullScreenImageIntent.putExtra(UiUtilities.EXTRA_FEEDS, adapter.getFeedItems());
                fullScreenImageIntent.putExtra(UiUtilities.EXTRA_POSITION, (Integer) view.getTag());
                startActivity(fullScreenImageIntent);
                break;

            case R.id.btnOk:
                int sortBy = dialogFilter.getSelectedSortByOption();
                int sortOrder = dialogFilter.getSelectedSortOrderOption();
                dismissFilterDialog();
                applyFilter(sortBy, sortOrder);
                break;

            case R.id.btnCancel:
                dismissFilterDialog();
                break;
        }
    }

    /**
     * Method to apply the sortBy and sortOrder filter.
     *
     * @param sortBy    the {@link SortBy} option
     * @param sortOrder the {@link SortOrder} option
     */
    private void applyFilter(final int sortBy, int sortOrder) {
        SimpleDateFormat dateFormat = null;
        switch (sortBy) {
            case R.id.rbDateTaken:
                dateFormat = new SimpleDateFormat(ImageFeedItem.DATE_TAKEN_FORMAT);
                this.sortBy = SortBy.DATE_TAKEN;
                break;

            case R.id.rbDatePublished:
                dateFormat = new SimpleDateFormat(ImageFeedItem.DATE_PUBLISHED_FORMAT);
                this.sortBy = SortBy.DATE_PUBLISHED;
                break;
        }

        switch (sortOrder) {
            case R.id.rbAscending:
                this.sortOrder = SortOrder.ASCENDING;
                break;

            case R.id.rbDescending:
                this.sortOrder = SortOrder.DESCENDING;
                break;
        }

        final SimpleDateFormat format = dateFormat;

        //User java collection's utility method sort to sort the data set "in place" and with order "n log n" complexity
        Collections.sort(adapter.getFeedItems(), new Comparator<ImageFeedItem>() {
            @Override
            public int compare(ImageFeedItem imageFeedItemLeft, ImageFeedItem imageFeedItemRight) {
                switch (ImageGalleryActivity.this.sortBy) {
                    case DATE_TAKEN:
                        switch (ImageGalleryActivity.this.sortOrder) {
                            case ASCENDING:
                                return DateTimeUtils.compareDates(imageFeedItemLeft.getDateTaken(), imageFeedItemRight.getDateTaken(), format);

                            case DESCENDING:
                                return DateTimeUtils.compareDates(imageFeedItemRight.getDateTaken(), imageFeedItemLeft.getDateTaken(), format);
                        }
                        break;

                    case DATE_PUBLISHED:
                        switch (ImageGalleryActivity.this.sortOrder) {
                            case ASCENDING:
                                return DateTimeUtils.compareDates(imageFeedItemLeft.getPublishedDate(), imageFeedItemRight.getPublishedDate(), format);

                            case DESCENDING:
                                return DateTimeUtils.compareDates(imageFeedItemRight.getPublishedDate(), imageFeedItemLeft.getPublishedDate(), format);
                        }
                        break;
                }
                return 0;
            }
        });

        //Refresh the list after filtering is complete
        adapter.notifyDataSetChanged();
    }

    /**
     * Method to check if write to sd card is permitted.
     *
     * @return true if write to sd card is permitted, false otherwise
     */
    private boolean canWriteToSdCard() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }

        return true;
    }

    /**
     * Method to request WRITE_EXTERNAL_STORAGE permission from settings.
     */
    private void requestWriteSDCardPermission() {
        CustomAlertDialog.showDialog(ImageGalleryActivity.this, R.string.dlgTitleMessage, getString(R.string.msgWriteSdCardPermissionRequired), R.string.dlgBtnOk, R.string.dlgBtnCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                CustomAlertDialog.dismiss(ImageGalleryActivity.this);
                switch (i) {
                    case DialogInterface.BUTTON_NEGATIVE:
                        ActivityCompat.requestPermissions(ImageGalleryActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_SD_CARD_PERMISSION);
                        break;
                }
            }
        });
    }

    @Override
    public void onCommunicationSuccess(CommunicationResponse response) {
        switch (response.getRequestCode()) {
            case CommunicationConstants.GET_FLICKR_FEEDS: {
                Message message = new Message();
                message.what = MSG_UPDATE_LIST;
                message.obj = response.getResponseObject();
                uiUpdateHandler.sendMessage(message);
            }
            break;

            case CommunicationConstants.GET_FEED_MAIN_IMAGE_URL: {
                Message message = new Message();
                message.what = MSG_DOWNLOAD_FEED_MAIN_IMAGE;
                message.obj = response.getResponseObject();
                uiUpdateHandler.sendMessage(message);
            }
            break;
        }
    }

    @Override
    public void onCommunicationFailure(CommunicationResponse error) {
        CustomProgressDialog.dismiss(ImageGalleryActivity.this);
        switch (error.getRequestCode()) {
            case CommunicationConstants.GET_FLICKR_FEEDS:
                CustomToast.show(ImageGalleryActivity.this, getString(R.string.errLoadingFeeds, error.getErrorDescription()), CustomToast.LENGTH_LONG);
                break;

            case CommunicationConstants.GET_FEED_MAIN_IMAGE_URL:
                CustomToast.show(ImageGalleryActivity.this, getString(R.string.errGettingFeedImageUrl, error.getErrorDescription()), CustomToast.LENGTH_LONG);
                break;
        }
    }

    /**
     * Method to download main (original quality) image from server and save it to device's pictures directory..
     *
     * @param imageFeedItem the {@link ImageFeedItem} object
     */
    private void downloadFeedImage(final ImageFeedItem imageFeedItem) {
        CustomProgressDialog.show(ImageGalleryActivity.this, getString(R.string.msgCachingImage), true);
        Glide.with(ImageGalleryActivity.this)
                .load(imageFeedItem.getImageUrl())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(imageFeedItem.getWidth(), imageFeedItem.getHeight()) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        Log.d(TAG, "onResourceReady: resource->" + resource);
                        //Save bitmap to gallery
                        String fileName = Uri.parse(imageFeedItem.getImageUrl()).getLastPathSegment();
                        ImageCacheUtils.saveImageToGallery(resource, fileName);

                        CustomProgressDialog.dismiss(ImageGalleryActivity.this);
                        CustomToast.show(ImageGalleryActivity.this, getString(R.string.msgImageSavedToGallery, fileName), CustomToast.LENGTH_LONG);
                    }
                });
    }

    @Override
    public void onBackPressed() {
        if (searchView != null && !searchView.isIconified()) {
            searchView.setIconified(true);
        } else {
            super.onBackPressed();
        }
    }
}