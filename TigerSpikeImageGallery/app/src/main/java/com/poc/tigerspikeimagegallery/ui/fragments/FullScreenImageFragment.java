package com.poc.tigerspikeimagegallery.ui.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.poc.tigerspikeimagegallery.R;
import com.poc.tigerspikeimagegallery.communication.CommunicationConstants;
import com.poc.tigerspikeimagegallery.communication.CommunicationResponse;
import com.poc.tigerspikeimagegallery.communication.ICommunicationCallback;
import com.poc.tigerspikeimagegallery.communication.webservices.GetFeedMainImageUrl;
import com.poc.tigerspikeimagegallery.datamodels.ImageFeedItem;
import com.poc.tigerspikeimagegallery.ui.utils.CustomToast;

/**
 * Fragment class for displaying feed image in full screen along with feed details.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class FullScreenImageFragment extends Fragment implements ICommunicationCallback {
    private static final String ARG_IMAGE_FEED_ITEM = "IMAGE_FEED_ITEM";
    private static final String ARG_POSITION = "POSITION";
    private ImageFeedItem imageFeedItem;

    private ImageView ivMain;

    private ProgressBar pbMain;
    private Handler uiUpdateHandler;

    private final int MSG_DOWNLOAD_FEED_MAIN_IMAGE = 101;
    private final int MSG_HIDE_PROGRESS = 102;

    public FullScreenImageFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given {@link ImageFeedItem} and position.
     *
     * @param item
     * @param position
     * @return a new instance of this fragment
     */
    public static FullScreenImageFragment newInstance(ImageFeedItem item, int position) {
        FullScreenImageFragment fragment = new FullScreenImageFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_IMAGE_FEED_ITEM, item);
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_full_screen_image, container, false);
        ivMain = (ImageView) rootView.findViewById(R.id.ivMain);
        pbMain = (ProgressBar) rootView.findViewById(R.id.pbMain);

        initializeUiUpdateHandler();

        Bundle args = getArguments();
        imageFeedItem = (ImageFeedItem) args.getSerializable(ARG_IMAGE_FEED_ITEM);
        int position = args.getInt(ARG_POSITION);

        final TextView tvDetails = (TextView) rootView.findViewById(R.id.tvDetails);
        tvDetails.setText(imageFeedItem.toString());

        if (TextUtils.isEmpty(imageFeedItem.getImageUrl())) {
            getFeedMainImageUrl(imageFeedItem);
        } else {
            displayImage();
        }

        ivMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvDetails.getVisibility() == View.VISIBLE) {
                    tvDetails.setVisibility(View.GONE);
                } else {
                    tvDetails.setVisibility(View.VISIBLE);
                }
            }
        });
        return rootView;
    }

    /**
     * Method to initialize the UI update handler.
     * Called when the webservice response is received in case of;
     * 1. Feed list
     * 2. Feed details
     */
    private void initializeUiUpdateHandler() {
        uiUpdateHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                switch (message.what) {
                    case MSG_DOWNLOAD_FEED_MAIN_IMAGE:
                        try {
                            displayImage();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;

                    case MSG_HIDE_PROGRESS:
                        try {
                            pbMain.setVisibility(View.GONE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;
                }
                return false;
            }
        });
    }

    /**
     * Method to get feed main image url and other details from server.
     *
     * @param imageFeedItem the {@link ImageFeedItem} object
     */
    private void getFeedMainImageUrl(ImageFeedItem imageFeedItem) {
        pbMain.setVisibility(View.VISIBLE);
        GetFeedMainImageUrl getFeedMainImageUrl = new GetFeedMainImageUrl(imageFeedItem, FullScreenImageFragment.this);
        getFeedMainImageUrl.send();
    }

    /**
     * Method to display feed original image using {@link Glide} APIs
     */
    private void displayImage() {
        Glide.with(FullScreenImageFragment.this)
                .load(imageFeedItem.getImageUrl())
                .thumbnail(0.25f)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        pbMain.setVisibility(View.GONE);
                        CustomToast.show(getActivity(), getString(R.string.errGettingFeedImageUrl, e.getMessage()), CustomToast.LENGTH_LONG);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        pbMain.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(ivMain);
    }

    @Override
    public void onCommunicationSuccess(CommunicationResponse response) {
        switch (response.getRequestCode()) {
            case CommunicationConstants.GET_FEED_MAIN_IMAGE_URL: {
                uiUpdateHandler.sendEmptyMessage(MSG_DOWNLOAD_FEED_MAIN_IMAGE);
            }
            break;
        }
    }

    @Override
    public void onCommunicationFailure(CommunicationResponse error) {
        switch (error.getRequestCode()) {
            case CommunicationConstants.GET_FEED_MAIN_IMAGE_URL:
                CustomToast.show(getActivity(), getString(R.string.errGettingFeedImageUrl, error.getErrorDescription()), CustomToast.LENGTH_LONG);
                uiUpdateHandler.sendEmptyMessage(MSG_HIDE_PROGRESS);
                break;
        }
    }
}
