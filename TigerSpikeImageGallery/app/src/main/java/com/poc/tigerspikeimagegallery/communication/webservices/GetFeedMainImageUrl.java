package com.poc.tigerspikeimagegallery.communication.webservices;

import android.util.Log;

import com.poc.tigerspikeimagegallery.MainApplication;
import com.poc.tigerspikeimagegallery.R;
import com.poc.tigerspikeimagegallery.communication.CommunicationConstants;
import com.poc.tigerspikeimagegallery.communication.CommunicationResponse;
import com.poc.tigerspikeimagegallery.communication.HttpCommunication;
import com.poc.tigerspikeimagegallery.communication.ICommunicationCallback;
import com.poc.tigerspikeimagegallery.communication.RequestParam;
import com.poc.tigerspikeimagegallery.datamodels.ImageFeedItem;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Webservice to get the feed image url and meta data.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class GetFeedMainImageUrl extends WebServiceBase {
    private final String TAG = GetFeedMainImageUrl.class.getSimpleName();
    private final String GET_FEED_MAIN_IMAGE_URL = "https://www.flickr.com/services/oembed?";

    private ImageFeedItem imageFeedItem;
    private final ICommunicationCallback callbackListener;

    /**
     * Parameterized Constructor
     *
     * @param imageFeedItem    the {@link ImageFeedItem} object
     * @param callbackListener the {@link ICommunicationCallback} listener object to notify success/failure events
     */
    public GetFeedMainImageUrl(ImageFeedItem imageFeedItem, ICommunicationCallback callbackListener) {
        this.imageFeedItem = imageFeedItem;
        this.callbackListener = callbackListener;
    }

    @Override
    protected String getUrl() {
        return GET_FEED_MAIN_IMAGE_URL;
    }

    @Override
    protected ArrayList<RequestParam> getRequestParams() {
        ArrayList<RequestParam> requestParams = new ArrayList<>();
        requestParams.add(new RequestParam("url", imageFeedItem.getFeedUrl()));
        requestParams.add(new RequestParam("format", "json"));
        return requestParams;
    }

    @Override
    protected int getRequestCode() {
        return CommunicationConstants.GET_FEED_MAIN_IMAGE_URL;
    }

    @Override
    protected HttpCommunication.CommunicationMethod getCommunicationMethod() {
        return HttpCommunication.CommunicationMethod.GET;
    }

    @Override
    protected void parseResponse(CommunicationResponse response) {
        String responseMessage = (String) response.getResponseObject();
        Log.d(TAG, "parseResponse: response->" + responseMessage);
        try {
            JSONObject responseJson = new JSONObject(responseMessage);
            String url = responseJson.getString("url");
            imageFeedItem.setImageUrl(url);

            int width = (int) responseJson.optDouble("width");
            imageFeedItem.setWidth(width);

            int height = (int) responseJson.optDouble("height");
            imageFeedItem.setHeight(height);

            response.setResponseObject(imageFeedItem);
            callbackListener.onCommunicationSuccess(response);
        } catch (Exception e) {
            e.printStackTrace();
            CommunicationResponse error = new CommunicationResponse(getRequestCode(), CommunicationConstants.ERROR_INVALID_RESPONSE, MainApplication.appContext.getString(R.string.errInvalidResponse));
            notifyError(error);
        }
    }

    @Override
    protected void notifyError(CommunicationResponse error) {
        Log.d(TAG, "parseResponse: response->" + error.getErrorDescription());
        callbackListener.onCommunicationFailure(error);
    }
}
