package com.poc.tigerspikeimagegallery.communication;

/**
 * Datamodel class used to hold the request/query params.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class RequestParam {
    private final String key, value;

    public RequestParam(String key, String value) {
        this.key = key;
        this.value = value;
    }

    /**
     * Method to get key
     *
     * @return
     */
    public String getKey() {
        return key;
    }

    /**
     * Method to get value
     *
     * @return
     */
    public String getValue() {
        return value;
    }
}
