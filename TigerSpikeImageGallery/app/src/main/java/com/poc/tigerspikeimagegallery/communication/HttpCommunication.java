package com.poc.tigerspikeimagegallery.communication;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * This class is used to perform HTTP communication using Android APIs.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class HttpCommunication {
    private static final String TAG = HttpCommunication.class.getSimpleName();

    public enum CommunicationMethod {
        GET, POST, PUT, DELETE
    }

    private static final int CONNECTION_TIMEOUT = 15 * 1000;


    /**
     * MEthod to send the HTTP request and get the response from server.
     *
     * @param webserviceUrl the webservice url/endpoint
     * @param method        the {@link CommunicationMethod}
     * @param requestParams the {@link ArrayList} of {@link RequestParam} objects
     * @return the received response from server.
     * @throws Exception
     */
    public String makeHttpRequest(String webserviceUrl, CommunicationMethod method, ArrayList<RequestParam> requestParams) throws Exception {

        InputStream inputStream = null;
        String response = "";

        if (requestParams != null && requestParams.size() > 0) {
            //connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            String encodedRequestParams = encodeRequestParams(requestParams);
            webserviceUrl += encodedRequestParams;
        }

        URL url = new URL(webserviceUrl);
        URLConnection connection = url.openConnection();
        connection.setConnectTimeout(CONNECTION_TIMEOUT);
        connection.setReadTimeout(CONNECTION_TIMEOUT);
        connection.setDoInput(true);

        ((HttpURLConnection) connection).setRequestMethod(method.name());

        connection.connect();

        inputStream = connection.getInputStream();

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        inputStream.close();
        response = sb.toString();
        ((HttpURLConnection) connection).disconnect();

        Log.d(TAG, "makeHttpRequest: response->" + response);
        return response;
    }

    /**
     * Method to encode the url params
     *
     * @param params the {@link ArrayList} of {@link RequestParam} objects
     * @return the url encoded string of key=value pairs for all request params
     */
    private String encodeRequestParams(ArrayList<RequestParam> params) {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (RequestParam requestParam : params) {
            if (first)
                first = false;
            else
                result.append("&");
            try {
                result.append(URLEncoder.encode(requestParam.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(requestParam.getValue(), "UTF-8"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result.toString();
    }
}