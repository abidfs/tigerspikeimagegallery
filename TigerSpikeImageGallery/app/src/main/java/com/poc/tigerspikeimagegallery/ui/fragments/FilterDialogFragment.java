package com.poc.tigerspikeimagegallery.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioGroup;

import com.poc.tigerspikeimagegallery.R;

/**
 * This class is used to display the filter dialog as a fragment.
 * <p>
 * Created by SAYYAD on 18-12-2016.
 */
public class FilterDialogFragment extends DialogFragment {
    private Dialog filterDialog;

    private View.OnClickListener onClickListener;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        filterDialog = new Dialog(getActivity());
        filterDialog.setCanceledOnTouchOutside(false);
        filterDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        filterDialog.setContentView(R.layout.dialog_filter);

        Button btnOK = (Button) filterDialog.findViewById(R.id.btnOk);
        btnOK.setOnClickListener(onClickListener);

        Button btnCancel = (Button) filterDialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(onClickListener);

        return filterDialog;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    /**
     * Method to get the selected sortBy radio button's id
     *
     * @return the selected sortBy radio button's id
     */
    public int getSelectedSortByOption() {
        RadioGroup rgSortBy = (RadioGroup) filterDialog.findViewById(R.id.rgSortBy);
        return rgSortBy.getCheckedRadioButtonId();
    }

    /**
     * Method to get the selected sortOrder radio button's id
     *
     * @return the selected sortOrder radio button's id
     */
    public int getSelectedSortOrderOption() {
        RadioGroup rgSortOrder = (RadioGroup) filterDialog.findViewById(R.id.rgSortOrder);
        return rgSortOrder.getCheckedRadioButtonId();
    }
}