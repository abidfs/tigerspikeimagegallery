package com.poc.tigerspikeimagegallery.datamodels;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Datamodel class to hold the information of flicker feed item
 * Created by SAYYAD on 17-12-2016.
 */
public class ImageFeedItem implements Serializable {
    private String title;
    private String feedUrl;
    private String thumbnailUrl;
    private String imageUrl;
    private String description;
    private String dateTaken;
    private String publishedDate;
    private String author;
    private String tags;
    private int width, height;

    public static final String DATE_TAKEN_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final String DATE_PUBLISHED_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    /**
     * Parameterized Constructor
     *
     * @param title         the feed title
     * @param feedUrl       the feed url
     * @param thumbnailUrl  the thumbnail url
     * @param imageUrl      the full size image url
     * @param description
     * @param dateTaken     the date on which photo is taken (yyyy-MM-dd'T'HH:mm:ssZ)
     * @param publishedDate the date when image was published to flickr (yyyy-MM-ddTHH:mm:ss'Z')
     * @param author        the author
     * @param tags          the space separated list of tags
     */
    public ImageFeedItem(String title, String feedUrl, String thumbnailUrl, String imageUrl, String description, String dateTaken, String publishedDate, String author, String tags) {
        this.title = title;
        this.feedUrl = feedUrl;
        this.thumbnailUrl = thumbnailUrl;
        this.imageUrl = imageUrl;
        this.description = description;
        this.dateTaken = dateTaken;
        this.publishedDate = publishedDate;
        this.author = author;
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFeedUrl() {
        return feedUrl;
    }

    public void setFeedUrl(String feedUrl) {
        this.feedUrl = feedUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(String dateTaken) {
        this.dateTaken = dateTaken;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(title);
        if (!TextUtils.isEmpty(dateTaken)) {
            builder.append("\nDate Taken: ");
            builder.append(dateTaken);
        }
        if (!TextUtils.isEmpty(publishedDate)) {
            builder.append("\nPublished On: ");
            builder.append(publishedDate);
        }

        if (!TextUtils.isEmpty(author)) {
            builder.append("\nAuthor: ");
            builder.append(author);
        }

        if (!TextUtils.isEmpty(tags)) {
            builder.append("\nTags: ");
            builder.append(tags);
        }

        if (width != 0 && height != 0) {
            builder.append("\nResolution: ");
            builder.append(width);
            builder.append(" x ");
            builder.append(height);
        }
        return builder.toString();
    }
}
