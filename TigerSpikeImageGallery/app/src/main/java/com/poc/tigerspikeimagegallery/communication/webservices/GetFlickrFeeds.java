package com.poc.tigerspikeimagegallery.communication.webservices;

import android.text.TextUtils;
import android.util.Log;

import com.poc.tigerspikeimagegallery.MainApplication;
import com.poc.tigerspikeimagegallery.R;
import com.poc.tigerspikeimagegallery.communication.CommunicationConstants;
import com.poc.tigerspikeimagegallery.communication.CommunicationResponse;
import com.poc.tigerspikeimagegallery.communication.HttpCommunication;
import com.poc.tigerspikeimagegallery.communication.ICommunicationCallback;
import com.poc.tigerspikeimagegallery.communication.RequestParam;
import com.poc.tigerspikeimagegallery.datamodels.ImageFeedItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Webservice class to get the flickr feed.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class GetFlickrFeeds extends WebServiceBase {
    private final String TAG = GetFlickrFeeds.class.getSimpleName();
    private final String GET_FLICKR_FEEDS_URL = "https://api.flickr.com/services/feeds/photos_public.gne?";

    private String tags;
    private final ICommunicationCallback callbackListener;

    /**
     * Parameterized Constructor.
     *
     * @param callbackListener the {@link ICommunicationCallback} object to notify success/failure events
     */
    public GetFlickrFeeds(ICommunicationCallback callbackListener) {
        this.callbackListener = callbackListener;
    }

    /**
     * Parameterized Constructor.
     *
     * @param tags             the space separated list of tags
     * @param callbackListener the {@link ICommunicationCallback} object to notify success/failure events
     */
    public GetFlickrFeeds(String tags, ICommunicationCallback callbackListener) {
        this.tags = tags;
        this.callbackListener = callbackListener;
    }

    @Override
    protected String getUrl() {
        return GET_FLICKR_FEEDS_URL;
    }

    @Override
    protected ArrayList<RequestParam> getRequestParams() {
        ArrayList<RequestParam> requestParams = new ArrayList<>();
        requestParams.add(new RequestParam("format", "json"));
        if (!TextUtils.isEmpty(tags)) {
            requestParams.add(new RequestParam("tags", tags.replace(' ', ',')));
        }
        return requestParams;
    }

    @Override
    protected int getRequestCode() {
        return CommunicationConstants.GET_FLICKR_FEEDS;
    }

    @Override
    protected HttpCommunication.CommunicationMethod getCommunicationMethod() {
        return HttpCommunication.CommunicationMethod.GET;
    }

    @Override
    protected void parseResponse(CommunicationResponse response) {
        String responseMessage = (String) response.getResponseObject();
        Log.d(TAG, "parseResponse: response->" + responseMessage);
        try {
            ArrayList<ImageFeedItem> imageFeedItems = new ArrayList<>();
            //GetFlickrFeeds Response is embeded between jsonFlickrFeed(..................)
            //Remove starting "jsonFlickrFeed(" and last ")" from response before converting it to json
            responseMessage = responseMessage.substring(responseMessage.indexOf('(') + 1, responseMessage.lastIndexOf(')'));
            JSONObject responseJson = new JSONObject(responseMessage);
            JSONArray feeds = responseJson.getJSONArray("items");

            int size = feeds.length();
            for (int i = 0; i < size; i++) {
                JSONObject feedJsonObj = feeds.getJSONObject(i);
                String title = feedJsonObj.optString("title");
                String feedUrl = feedJsonObj.optString("link");
                String thumbnailUrl = feedJsonObj.getJSONObject("media").getString("m");
                String imageUrl = null;
                //TODO parse description
                String description = null;
                String dateTaken = feedJsonObj.optString("date_taken");
                String publishedData = feedJsonObj.optString("published");
                String author = feedJsonObj.optString("author");
                if (!TextUtils.isEmpty(author)) {
                    //Extract the author name correctly for eg. "author": "nobody@flickr.com (\"ich2651\")"
                    author = author.substring(author.indexOf("(\"") + 2, author.indexOf("\")"));
                }
                String tags = feedJsonObj.optString("tags");

                ImageFeedItem imageFeedItem = new ImageFeedItem(title, feedUrl, thumbnailUrl, imageUrl, description, dateTaken, publishedData, author, tags);
                imageFeedItems.add(imageFeedItem);
            }

            response.setResponseObject(imageFeedItems);

            callbackListener.onCommunicationSuccess(response);
        } catch (Exception e) {
            e.printStackTrace();
            CommunicationResponse error = new CommunicationResponse(getRequestCode(), CommunicationConstants.ERROR_INVALID_RESPONSE, MainApplication.appContext.getString(R.string.errInvalidResponse));
            notifyError(error);
        }
    }

    @Override
    protected void notifyError(CommunicationResponse error) {
        Log.d(TAG, "parseResponse: response->" + error.getErrorDescription());
        callbackListener.onCommunicationFailure(error);
    }
}
