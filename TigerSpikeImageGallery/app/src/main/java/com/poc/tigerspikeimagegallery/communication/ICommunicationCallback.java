package com.poc.tigerspikeimagegallery.communication;

/**
 * This Interface is used to provide communication success/failure callbacks to application.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public interface ICommunicationCallback {
    /**
     * Callback method to notify communication success event.
     *
     * @param response the {@link CommunicationResponse} object
     */
    void onCommunicationSuccess(CommunicationResponse response);


    /**
     * Callback method to notify communication failure event.
     *
     * @param error the {@link CommunicationResponse} object
     */
    void onCommunicationFailure(CommunicationResponse error);
}
