package com.poc.tigerspikeimagegallery.ui.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.poc.tigerspikeimagegallery.R;
import com.poc.tigerspikeimagegallery.datamodels.ImageFeedItem;
import com.poc.tigerspikeimagegallery.ui.adapters.FullScreenImagePagerAdapter;
import com.poc.tigerspikeimagegallery.ui.utils.UiUtilities;

import java.util.ArrayList;

/**
 * Activity class to show the slideshow of images in their original quality.
 * Contains {@link ViewPager}.
 * User can swipe left/right to change the image.
 */
public class FullScreenImageActivity extends AppCompatActivity {

    private FullScreenImagePagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the {@link ImageFeedItem} contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayList<ImageFeedItem> imageFeedItems = (ArrayList<ImageFeedItem>) getIntent().getSerializableExtra(UiUtilities.EXTRA_FEEDS);
        int position = getIntent().getIntExtra(UiUtilities.EXTRA_POSITION, 0);

        // Create the adapter that will return a fragment for each of the ImageFeedItem in list
        mSectionsPagerAdapter = new FullScreenImagePagerAdapter(getSupportFragmentManager(), imageFeedItems);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(position);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_full_screen_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            //TODO handled share button click
        }

        return super.onOptionsItemSelected(item);
    }
}