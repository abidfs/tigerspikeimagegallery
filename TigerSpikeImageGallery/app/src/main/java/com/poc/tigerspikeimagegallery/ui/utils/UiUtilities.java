package com.poc.tigerspikeimagegallery.ui.utils;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.view.inputmethod.InputMethodManager;

import com.poc.tigerspikeimagegallery.MainApplication;

/**
 * Utility class for helper UI functions and constants.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class UiUtilities {

    public static final String EXTRA_FEEDS = "FEEDS";
    public static final String EXTRA_POSITION = "POSITION";

    /**
     * Method to hide soft keyboard from window
     *
     * @param searchView the currently focused {@link SearchView}
     */
    public static void hideSoftKeyboard(SearchView searchView) {
        InputMethodManager inputMethodManager = (InputMethodManager) MainApplication.appContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
    }
}
