package com.poc.tigerspikeimagegallery.ui.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Utility class to show/dismiss alert dialog.
 * <p>
 * Created by SAYYAD on 17-12-2016.
 */
public class CustomAlertDialog {
    private static AlertDialog.Builder builder;
    private static AlertDialog alertDialog;

    /**
     * Method to display dialog.
     *
     * @param activity                  the activity context
     * @param title                     the string resource id of dialog title
     * @param message                   the string message
     * @param btnLeft                   the string resource id of dialog negative button
     * @param btnRight                  the string resource id of dialog positive button
     * @param dialogButtonClickListener the {@link DialogInterface.OnClickListener} to handle dialog button click
     *                                  events.
     */
    public static void showDialog(final Activity activity, final int title, final String message, final int btnLeft, final int btnRight,
                                  final DialogInterface.OnClickListener dialogButtonClickListener) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (alertDialog != null && alertDialog.isShowing()) {
                        alertDialog.dismiss();
                    }

                    builder = new AlertDialog.Builder(activity);
                    builder.setCancelable(false);
                    builder.setTitle(title);
                    builder.setMessage(message);
                    if (btnLeft != -1)
                        builder.setNegativeButton(btnLeft, dialogButtonClickListener);
                    if (btnRight != -1)
                        builder.setPositiveButton(btnRight, dialogButtonClickListener);
                    alertDialog = builder.create();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public static void dismiss(Activity activity) {
        try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {

        }
    }
}
